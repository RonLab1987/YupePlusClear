var gulp = require('gulp');
var sass = require('gulp-sass');
var csso = require('gulp-csso');

//var path='./themes/default/web';
var path='./public_html/web';


gulp.task('path',function(){
    console.log(path+'/css');
});

gulp.task('sass',function(){
    gulp.src(path+'/scss/**/*.scss')
            .pipe(sass().on('error', sass.logError))
            .pipe(csso()) 
            .pipe(gulp.dest(path+'/css'));
});

gulp.task('watch',function(){
    gulp.watch(path+'/scss/**/*.scss',['sass']);
});
    